## AngularJS Technical Test ##

# Introduction

Your mission, should you choose to accept it is:

- The technical test phase of your interview should take no longer than 2 hours for you to complete.  

- Whilst fully completing the task is preferable our opinion is based on the quality of your code and your technical approach. If you don’t complete the task then please be prepared to talk us through how you would go about doing so. 

- The supplied solution is as follows:
	- ToDoList – base MVC5 Application with WebAPI2 – VS-Scaffolded
	- ToDoList.AcceptanceTests – SpecFlow (and SpecSalad), NUnit, Fluent Assertions, Selenium and Karma NuGet packages have been installed
	- ToDoList.Test  - NUnit and  Fluent Assertions NuGet packages have been installed
	- There is a WebAPI (API/ToDoList) that will return the contents of the supplied XML ToDo list

# Requirements

The ToDo list should be presented to the user utilizing your preferred AngularJS approach.
The user should be able to add a new entry to the list.
Validation of the data should be performed.
The updated list should be persisted back to the XML file.
Success Criteria

- The list should be presented via AngularJS and utilize an Angular service to obtain the data.

- User input should be validated

- Additions to the displayed list should be persisted to the supplied XML.

- You are expected to utilize and demonstrate:
	- A test-driven approach
	- SOLID principles
	- Knowledge of AngularJS

# Optional Criteria

- The application being developed is using BDD – specifically SpecSalad – creating an end-to-end test for displaying or adding to the list would be beneficial but not essential. 

- During the face-to-face interview, we may request a 30-minute paring session and ask you to extend the code. Details will be given at the time. 
