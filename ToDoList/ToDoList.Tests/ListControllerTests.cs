﻿using System;
using System.Linq;
using System.Web.Http.Results;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using ToDoList.Controllers;
using ToDoList.DomainServices;
using ToDoListModel = ToDoList.Models.ToDoList;

namespace ToDoList.Tests
{
    [TestFixture]
    public class ListControllerTests
    {
        private const string TestFileName = "ToDoListTest.xml";

        private ToDoListController _controller;
        private Mock<IPathResolver> _pathResolver;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            // Reset test file here.
        }

        [SetUp]
        public void Setup()
        {
            _pathResolver = new Mock<IPathResolver>();
            _controller = new ToDoListController(_pathResolver.Object);
        }

        [Test]
        public void CanRequestTasks()
        {
            _pathResolver.Setup(r => r.Resolve(It.IsAny<string>()))
                .Returns(TestFileName);

            var result = _controller.Get().ToList();

            _pathResolver.Verify(r => r.Resolve(It.IsAny<string>()));

            result.Should().NotBeNull();
            result.Should().NotBeEmpty();
        }

        [Test]
        public void CanAddToLoadListTasks()
        {
            _pathResolver.Setup(r => r.Resolve(It.IsAny<string>()))
                .Returns(TestFileName);

            var expectedDescription = "test";

            ToDoListModel task = new ToDoListModel
            {
                Task = expectedDescription
            };

            var result = _controller.Post(task) as JsonResult<ToDoListModel>;

            result.Content.Task.Should().Be(expectedDescription);

            var expectedDate = DateTime.Now.AddDays(7).Date;

            result.Content.DueDate.Date.Should().Be(expectedDate);
        }
    }
}