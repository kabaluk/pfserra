﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ToDoList.DomainServices;

namespace ToDoList.Controllers
{
    public class ToDoListController : ApiController
    {
        private readonly IPathResolver _pathResolver;

        public ToDoListController(IPathResolver pathResolver)
        {
            _pathResolver = pathResolver;
        }

        public IEnumerable<Models.ToDoList> Get()
        {
            string path = _pathResolver.Resolve(@"Xml\ToDoList.xml");

            var loadList = new LoadList(path);

            var tasks = loadList.Load();

            return tasks;
        }

        public IHttpActionResult Post(Models.ToDoList task)
        {
            // Could use a date provider to be testable as well
            task.DueDate = DateTime.Now.AddDays(7);

            string path = _pathResolver.Resolve(@"Xml\ToDoList.xml");

            var loadList = new LoadList(path);

            loadList.AddTask(task);

            return Json(task);
        }
    }
}