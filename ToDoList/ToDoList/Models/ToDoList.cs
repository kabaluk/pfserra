﻿using System;

namespace ToDoList.Models
{
    public class ToDoList
    {
        public int Id { get; set; }

        public string Task { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime? CompletedDate { get; set; }
    }
}