﻿namespace ToDoList.DomainServices
{
    public interface IPathResolver
    {
        string Resolve(string localPath);
    }
}