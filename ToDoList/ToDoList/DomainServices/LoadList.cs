﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace ToDoList.DomainServices
{
    public class LoadList
    {
        private readonly string _path;

        public LoadList(string path)
        {
            _path = path;
        }

        public List<Models.ToDoList> Load()
        {
            var xdoc = XDocument.Load(_path);
            return xdoc.Descendants("ArrayOfToDoList")
                .Descendants("ToDoList")
                .Select(toDoItem => new Models.ToDoList
                {
                    Id = Convert.ToInt32(toDoItem.Element("Id").Value),
                    Task = toDoItem.Element("Task").Value,
                    DueDate = Convert.ToDateTime(toDoItem.Element("DueDate").Value),
                })
                .ToList();
        }

        public void AddTask(Models.ToDoList task)
        {
            var xdoc = XDocument.Load(_path);

            var newTask = new XElement(
                "ToDoList",
                new XElement("CompletedDate", task.CompletedDate),
                new XElement("DueDate", task.DueDate),
                new XElement("Id", task.Id),
                new XElement("Task", task.Task));

            xdoc.Element("ArrayOfToDoList").Add(newTask);

            xdoc.Save(_path);
        }
    }
}