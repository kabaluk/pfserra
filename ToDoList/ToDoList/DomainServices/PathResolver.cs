using System.IO;
using System.Web;

namespace ToDoList.DomainServices
{
    public class PathResolver : IPathResolver
    {
        public string Resolve(string localPath)
        {
            return Path.Combine(HttpContext.Current.Server.MapPath(@"~"), @"Xml\ToDoList.xml");
        }
    }
}